package com.begamer.card.common;

public class CommParams
{
	private static CommParams instance;
	//返回游戏key
	private String pay_game_key;
	//渠道支付key
	private String pay_key_gc;
	private String pay_key_cpyy;
	private String pay_key_tb;
	private String pay_key_dl;
	private String pay_key_coolpad;
	private String appId_91;
	private String pay_key_91;
	private String pay_key_bddk;
	private String pay_key_ky;
	
	private String talkingDataAppId;
	
	public void init()
	{
		if(instance==null)
		{
			instance=this;
		}
	}
	public static CommParams getInstance()
	{
		return instance;
	}
	public String getPay_game_key()
	{
		return pay_game_key;
	}
	public void setPay_game_key(String payGameKey)
	{
		pay_game_key = payGameKey;
	}
	public String getPay_key_gc()
	{
		return pay_key_gc;
	}
	public void setPay_key_gc(String payKeyGc)
	{
		pay_key_gc = payKeyGc;
	}
	public String getPay_key_cpyy()
	{
		return pay_key_cpyy;
	}
	public void setPay_key_cpyy(String payKeyCpyy)
	{
		pay_key_cpyy = payKeyCpyy;
	}
	public String getTalkingDataAppId()
	{
		return talkingDataAppId;
	}
	public void setTalkingDataAppId(String talkingDataAppId)
	{
		this.talkingDataAppId = talkingDataAppId;
	}
	public void setPay_key_tb(String pay_key_tb)
	{
		this.pay_key_tb = pay_key_tb;
	}
	public String getPay_key_tb()
	{
		return pay_key_tb;
	}
	public void setPay_key_dl(String pay_key_dl)
	{
		this.pay_key_dl = pay_key_dl;
	}
	public String getPay_key_dl()
	{
		return pay_key_dl;
	}
	public String getPay_key_coolpad()
	{
		return pay_key_coolpad;
	}
	public void setPay_key_coolpad(String payKeyCoolpad)
	{
		pay_key_coolpad = payKeyCoolpad;
	}
	public String getAppId_91()
	{
		return appId_91;
	}
	public void setAppId_91(String appId_91)
	{
		this.appId_91 = appId_91;
	}
	public String getPay_key_91()
	{
		return pay_key_91;
	}
	public void setPay_key_91(String payKey_91)
	{
		pay_key_91 = payKey_91;
	}
	public String getPay_key_bddk()
	{
		return pay_key_bddk;
	}
	public void setPay_key_bddk(String payKeyBddk)
	{
		pay_key_bddk = payKeyBddk;
	}
	public String getPay_key_ky()
	{
		return pay_key_ky;
	}
	public void setPay_key_ky(String payKeyKy)
	{
		pay_key_ky = payKeyKy;
	}
}
