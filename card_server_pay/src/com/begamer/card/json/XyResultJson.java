/**
 * 
 */
package com.begamer.card.json;

/**
 * @author LiTao 2014-8-21 下午09:24:16
 */
public class XyResultJson
{
	public String ret;
	public String msg;

	public String getRet()
	{
		return ret;
	}

	public void setRet(String ret)
	{
		this.ret = ret;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

}
