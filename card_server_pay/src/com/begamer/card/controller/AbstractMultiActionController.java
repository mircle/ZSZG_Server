package com.begamer.card.controller;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * 
 * @ClassName: AbstractMultiActionController
 * @Description: TODO
 * @author gs
 * @date Nov 1, 2011 1:43:48 PM
 * 
 */
public abstract class AbstractMultiActionController extends MultiActionController {
}
