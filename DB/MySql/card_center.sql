/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : card_center

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2016-02-19 09:59:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for announce
-- ----------------------------
DROP TABLE IF EXISTS `announce`;
CREATE TABLE `announce` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ANNOUNCE` varchar(10000) DEFAULT '' COMMENT '公告',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of announce
-- ----------------------------

-- ----------------------------
-- Table structure for gift
-- ----------------------------
DROP TABLE IF EXISTS `gift`;
CREATE TABLE `gift` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) DEFAULT NULL COMMENT '礼包名字',
  `GOLD` int(11) DEFAULT '0' COMMENT '金币',
  `CRYSTAL` int(11) DEFAULT '0' COMMENT '水晶',
  `RUNENUM` int(11) DEFAULT '0' COMMENT '符文值',
  `POWER` int(11) DEFAULT '0' COMMENT '体力',
  `CARD` varchar(128) DEFAULT '',
  `SKILL` varchar(128) DEFAULT '',
  `PSKILL` varchar(128) DEFAULT '',
  `EQUIP` varchar(128) DEFAULT '',
  `ITEM` varchar(128) DEFAULT '',
  `STARTDATE` varchar(30) DEFAULT NULL COMMENT '开始时间',
  `ENDDATE` varchar(30) DEFAULT NULL COMMENT '结束时间',
  `UPDATEDATE` varchar(30) DEFAULT NULL COMMENT '更新时间',
  `NUM` int(11) DEFAULT '0' COMMENT '激活码数量',
  `FINALNUM` int(11) DEFAULT '0' COMMENT '激活码使用量',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gift
-- ----------------------------

-- ----------------------------
-- Table structure for gm_manager
-- ----------------------------
DROP TABLE IF EXISTS `gm_manager`;
CREATE TABLE `gm_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serverId` int(11) DEFAULT '0' COMMENT '服务器ID',
  `name` varchar(20) DEFAULT '' COMMENT '用户名',
  `password` varchar(20) DEFAULT '' COMMENT '密码',
  `playerId` int(11) DEFAULT '0' COMMENT '游戏账号',
  `createon` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gm_manager
-- ----------------------------
INSERT INTO `gm_manager` VALUES ('1', '2', 'admin', '123456', '2', '2014-04-14 13:33:58');
INSERT INTO `gm_manager` VALUES ('9', '2', 'shengyou', '123456', '0', '');

-- ----------------------------
-- Table structure for key_code
-- ----------------------------
DROP TABLE IF EXISTS `key_code`;
CREATE TABLE `key_code` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) DEFAULT '' COMMENT '激活码',
  `SERVERID` int(11) DEFAULT '0' COMMENT '领取者的服务器id',
  `USERID` int(11) DEFAULT '0' COMMENT '领取者userid',
  `PLAYERID` int(11) DEFAULT '0' COMMENT '玩家id',
  `STATUS` int(11) DEFAULT '0' COMMENT '领取状态 0未领取 1已领取',
  `RECEIVETIME` varchar(30) DEFAULT NULL COMMENT '领取时间',
  `GIFTID` int(11) DEFAULT '0' COMMENT '对应的礼包id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of key_code
-- ----------------------------
