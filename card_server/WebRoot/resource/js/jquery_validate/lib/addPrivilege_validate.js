$( function() {
	$("#privilegeForm").validate( {
		success : function(label) {
			label.addClass("success").text("输入正确!");
		},
		rules : {
			serverId : {
				required :true
			},
			managerId : {
				required :true
			}
		},
		messages : {
			serverId : {
				required :"请选择区服"
			},
			managerId : {
				required :"请选择GM"
			}
		}
	});
});