package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class RuneJson extends BasicJson
{
	/**0表示打开符文界面,其他值代表符文页**/
	public int page;
	
	public int getPage()
	{
		return page;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
}
