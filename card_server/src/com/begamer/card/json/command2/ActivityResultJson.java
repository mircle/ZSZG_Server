package com.begamer.card.json.command2;

import java.util.List;


import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ActivityElement;

public class ActivityResultJson extends ErrorJson
{
	public List<ActivityElement> acts;//活动列表

	public List<ActivityElement> getActs() {
		return acts;
	}

	public void setActs(List<ActivityElement> acts) {
		this.acts = acts;
	}
	
	
}
