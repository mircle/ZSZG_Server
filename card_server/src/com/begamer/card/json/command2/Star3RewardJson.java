package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class Star3RewardJson extends BasicJson
{
	public String m;

	public String getM()
	{
		return m;
	}

	public void setM(String m)
	{
		this.m = m;
	}
	
}
