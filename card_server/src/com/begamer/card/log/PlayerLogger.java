package com.begamer.card.log;

import org.apache.log4j.Logger;

/**
 * 玩家日志
 * @author LiTao
 * 2014-1-27 上午10:12:21
 */
public class PlayerLogger
{
	public static final Logger logger=Logger.getLogger("PlayerLogger");
	
	public static void print(String s)
	{
		logger.info(s);
	}
}
