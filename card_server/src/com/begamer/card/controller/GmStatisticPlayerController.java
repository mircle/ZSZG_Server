package com.begamer.card.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.begamer.card.cache.Cache;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.model.dao.GmPlayerDao;
import com.begamer.card.model.dao.GmStatisticPlayerDao;
import com.begamer.card.model.pojo.PageUtil;
import com.begamer.card.model.pojo.PkRank;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.view.PlayerStatisticView;

public class GmStatisticPlayerController extends AbstractMultiActionController {

	@Autowired
	private GmPlayerDao gmPlayerDao;
	@Autowired
	private GmStatisticPlayerDao gmStatisticPlayerDao;

	/** 首页* */
	/** 数据统计* */
	public ModelAndView statistic(HttpServletRequest request,
			HttpServletResponse response)
	{
		try
		{
			// 获取当前页数
			int currentpage = RequestUtil.GetParamInteger(request,
					"pageCurrent");
			if (currentpage == 0)
			{
				currentpage = 1;
			}
			// 获取数据的总条数
			int count = gmPlayerDao.findAllPlayer().size();
			// 创建分页实体
			PageUtil page = new PageUtil(count, 100, currentpage);
			List<Player> list = gmPlayerDao.getAllByPage(page.getCurrentPage(),
					page.getPageSize());
			// 优先从在线玩家取
			if (list != null)
			{
				for (int k = 0; k < list.size(); k++)
				{
					Player player = list.get(k);
					PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(
							player.getId());
					if (pi != null)
					{
						list.remove(k);
						list.add(k, pi.player);
					}
				}
			}
			List<PkRank> pkRanks = gmStatisticPlayerDao.getPkRanks();
			List<PlayerStatisticView> pList = new ArrayList<PlayerStatisticView>();
			for (int i = 0; i < list.size(); i++)
			{
				Player player = list.get(i);
				for (int j = 0; j < pkRanks.size(); j++)
				{
					PkRank pkRank = pkRanks.get(j);
					if (player.getId() == pkRank.getPlayerId())
					{
						PlayerStatisticView playerStatisticView = PlayerStatisticView
								.createPlayerStatisticView(player.getId(),
										player.getName(), player.getLevel(),
										player.getCrystal(), player.getCrystalPay(), pkRank.getRank(),
										player.getVipLevel(), player
												.getBattlePower(), player
												.getSignTimes(), player
												.getLastSignTime());
						pList.add(playerStatisticView);
					}
				}
			}
			String[] num = new String[page.getTotalPage()];
			String href = "gmtj.htm?action=statistic&pageCurrent=";
			request.setAttribute("page", page);
			request.setAttribute("num", num);
			request.setAttribute("myherf", href);
			request.setAttribute("list", pList);

			/** 统计玩家在线人数* */
			int online = Cache.getInstance().getOnLinePlayer();
			/** 统计注册玩家人数* */
			int userNum = gmPlayerDao.getUserNum();

			request.setAttribute("online", online);
			request.setAttribute("userNum", userNum);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return new ModelAndView("statistic/statistic.vm");
	}

	/** 玩家等级排序* */
	public ModelAndView orderBy(HttpServletRequest request,
			HttpServletResponse response)
	{
		try
		{
			// // 获取当前页数
			// int currentpage = RequestUtil.GetParamInteger(request,
			// "pageCurrent");
			// if (currentpage == 0)
			// {
			// currentpage = 1;
			// }
			// // 获取数据的总条数
			// int count = gmPlayerDao.findAllPlayer().size();
			// // 创建分页实体
			// PageUtil page = new PageUtil(count, 10, currentpage);
			// List<Player> list =
			// gmPlayerDao.getAllByPage(page.getCurrentPage(),
			// page.getPageSize());
			List<Player> playerlist = null;
			List<PkRank> pkRankList = null;
			String types = request.getParameter("type").trim();
			byte[] type = types.getBytes();
			switch (type[0]) {
			case 'a':
				playerlist = gmStatisticPlayerDao.levelUp();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'b':
				playerlist = gmStatisticPlayerDao.levelDown();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'c':
				playerlist = gmStatisticPlayerDao.battlePowerUp();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'd':
				playerlist = gmStatisticPlayerDao.battlePowerDown();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'g':
				break;
			case 'h':
				break;
			case 'i':
				break;
			case 'j':
				break;
			case 'k':
				playerlist = gmStatisticPlayerDao.crystalUp();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'l':
				playerlist = gmStatisticPlayerDao.crystalDown();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'm':
				playerlist = gmStatisticPlayerDao.vipLevelUp();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'n':
				playerlist = gmStatisticPlayerDao.vipLevelDown();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'o':
				playerlist = gmStatisticPlayerDao.signTimesUp();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			case 'p':
				playerlist = gmStatisticPlayerDao.signTimesDown();
				pkRankList = gmStatisticPlayerDao.getPkRanks();
				break;
			}

			// 优先从在线玩家取
			if (playerlist != null)
			{
				for (int k = 0; k < playerlist.size(); k++)
				{
					Player player = playerlist.get(k);
					PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(
							player.getId());
					if (pi != null)
					{
						playerlist.remove(k);
						playerlist.add(k, pi.player);
					}
				}
			}
			List<PlayerStatisticView> pList = new ArrayList<PlayerStatisticView>();
			for (int i = 0; i < playerlist.size(); i++)
			{
				Player player = playerlist.get(i);
				for (int j = 0; j < pkRankList.size(); j++)
				{
					PkRank pkRank = pkRankList.get(j);
					if (pkRank.getPlayerId() == player.getId())
					{
						PlayerStatisticView playerStatisticView = PlayerStatisticView
								.createPlayerStatisticView(player.getId(),
										player.getName(), player.getLevel(),
										player.getCrystal(), player.getCrystalPay(), pkRank.getRank(),
										player.getVipLevel(), player
												.getBattlePower(), player
												.getSignTimes(), player
												.getLastSignTime());
						pList.add(playerStatisticView);
					}
				}
			}
			// String[] num = new String[page.getTotalPage()];
			// String href = "gmtj.htm?action=statistic&pageCurrent=";
			// request.setAttribute("page", page);
			// request.setAttribute("num", num);
			// request.setAttribute("myherf", href);
			request.setAttribute("list", pList);

			/** 统计玩家在线人数* */
			int online = Cache.getInstance().getOnLinePlayer();
			/** 统计注册玩家人数* */
			int userNum = gmPlayerDao.getUserNum();

			request.setAttribute("online", online);
			request.setAttribute("userNum", userNum);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("statistic/statistic2.vm");
	}

	public ModelAndView pkRankOrderBy(HttpServletRequest request,
			HttpServletResponse response)
	{
		try
		{
			List<Player> playerlist = null;
			List<PkRank> pkRankList = null;
			String types = request.getParameter("type").trim();
			byte[] type = types.getBytes();
			switch (type[0]) {
			case 'e':
				playerlist = gmPlayerDao.findAllPlayer();
				pkRankList = gmStatisticPlayerDao.pkRankUp();
				break;
			case 'f':
				playerlist = gmPlayerDao.findAllPlayer();
				pkRankList = gmStatisticPlayerDao.pkRankDown();
			}
			// 优先从在线玩家取
			if (playerlist != null)
			{
				for (int k = 0; k < playerlist.size(); k++)
				{
					Player player = playerlist.get(k);
					PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(
							player.getId());
					if (pi != null)
					{
						playerlist.remove(k);
						playerlist.add(k, pi.player);
					}
				}
			}
			List<PlayerStatisticView> pList = new ArrayList<PlayerStatisticView>();
			for (int j = 0; j < pkRankList.size(); j++)
			{
				PkRank pkRank = pkRankList.get(j);
				for (int i = 0; i < playerlist.size(); i++)
				{
					Player player = playerlist.get(i);
					if (pkRank.getPlayerId() == player.getId())
					{
						PlayerStatisticView playerStatisticView = PlayerStatisticView
								.createPlayerStatisticView(player.getId(),
										player.getName(), player.getLevel(),
										player.getCrystal(), player.getCrystalPay(), pkRank.getRank(),
										player.getVipLevel(), player
												.getBattlePower(), player
												.getSignTimes(), player
												.getLastSignTime());
						pList.add(playerStatisticView);
					}
				}
			}
			// request.setAttribute("myherf", href);
			request.setAttribute("list", pList);

			/** 统计玩家在线人数* */
			int online = Cache.getInstance().getOnLinePlayer();
			/** 统计注册玩家人数* */
			int userNum = gmPlayerDao.getUserNum();

			request.setAttribute("online", online);
			request.setAttribute("userNum", userNum);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("statistic/statistic2.vm");
	}

}
