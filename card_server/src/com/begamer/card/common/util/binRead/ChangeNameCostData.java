package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class ChangeNameCostData implements PropertyReader
{
	public int times;
	public int cost;
	
	private static HashMap<Integer, ChangeNameCostData> data =new HashMap<Integer, ChangeNameCostData>();
	@Override
	public void addData()
	{
		data.put(times, this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据times获取data**/
	public static ChangeNameCostData getChangeNameCostData(int index)
	{
		return data.get(index);
	}

}
