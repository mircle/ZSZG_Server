package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.Constant;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.CardJson;
import com.begamer.card.log.ErrorLogger;

public class MissionData implements PropertyReader
{
	/**关卡ID**/
	public int id;
	/**1普通关卡,2BOSS关卡**/
	/**关卡名称**/
	public String name;
	/**关卡地图**/
	public int map;
	/**区域**/
	public int zone;
	/**关卡类型**/
	public int missiontype;
	/**区域名称**/
	public String zonename;
	/**场景**/
	public String scene;
	/**类型**/
	public int type;
	/**出现条件**/
	public int unlockshowup;
	/**进入等级**/
	public int unlocklevel;
	/**boss头像**/
	public String bossicon;
	/**条件说明**/
	public String description;
	/**消耗体力**/
	public int cost;
	/**进入次数**/
	public int times;
	/**6个怪的数据**/
	public String[] monsters;
	/**BOSS技能和cd**/
	public String[] bossSkills;
	/**人物经验**/
	public int personexp;
	/**卡牌经验**/
	public int cardexp;
	/**掉落金币**/
	public int coins;
	/**掉落物品**/
	public List<String> drops;
	/**特殊奖励类别1**/
	public int specialtype1;
	/**特殊奖励1**/
	public String specialaward1;
	/**特殊奖励类别2**/
	public int specialtype2;
	/**特殊奖励2**/
	public String specialaward2;
	/**BONUS增长**/
	public int bonusGrow;
	/**BONUS减少**/
	public int bonusReduce;
	/**持续时间**/
	public int lastTime;
	
	public int addtasktype;//任务类型
	public int showtype;//显示类型
	public int addtaskid;//任务需求
	public int kopoint;//ko积分
	
	
	private static HashMap<Integer, MissionData> data=new HashMap<Integer, MissionData>();
	/**dataList一定是按id从小到大的顺序排列的**/
	private static List<MissionData> dataList=new ArrayList<MissionData>();
	
	private static final Logger errorlogger=ErrorLogger.logger;
	
	public void addData()
	{
		data.put(id,this);
		dataList.add(this);
	}
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}
	public static MissionData getMissionData(int targetId)
	{
		return data.get(targetId);
	}
	public void parse(String[] ss)
	{
		int location=0;
		id=StringUtil.getInt(ss[location]);
		name = StringUtil.getString(ss[location+1]);
		map = StringUtil.getInt(ss[location+2]);
		zone = StringUtil.getInt(ss[location+3]);
		missiontype = StringUtil.getInt(ss[location+4]);
		zonename = StringUtil.getString(ss[location+5]);
		scene =StringUtil.getString(ss[location+6]);
		type=StringUtil.getInt(ss[location+7]);
		unlockshowup = StringUtil.getInt(ss[location+8]);
		unlocklevel=StringUtil.getInt(ss[location+9]);
		bossicon =StringUtil.getString(ss[location+10]);
		description = StringUtil.getString(ss[location+11]);
		cost = StringUtil.getInt(ss[location+12]);
		times=StringUtil.getInt(ss[location+13]);
		
		monsters=new String[6];
		for(int i=0;i<6;i++)
		{
			location=14+i*7;
			int monster=StringUtil.getInt(ss[location]);
			int level=StringUtil.getInt(ss[location+1]);
			int atk=StringUtil.getInt(ss[location+2]);
			int def=StringUtil.getInt(ss[location+3]);
			int hp=StringUtil.getInt(ss[location+4]);
			String skill=StringUtil.getString(ss[location+5]);
			int BOSS=StringUtil.getInt(ss[location+6]);
			String monsterInfo=monster+"-"+level+"-"+atk+"-"+def+"-"+hp+"-"+skill+"-"+BOSS;
			monsters[i]=monsterInfo;
		}
		
		bossSkills=new String[3];
		for(int i=0;i<3;i++)
		{
			location=14+7*6+i*2;
			int bossSkill=StringUtil.getInt(ss[location]);
			int cd=StringUtil.getInt(ss[location+1]);
			bossSkills[i]=bossSkill+"-"+cd;
		}
		location=14+7*6+3*2;
		personexp=StringUtil.getInt(ss[location]);
		cardexp=StringUtil.getInt(ss[location+1]);
		coins=StringUtil.getInt(ss[location+2]);
		drops=new ArrayList<String>();
		for(int i=0;i<6;i++)
		{
			location=14+7*6+3*2+3+i*3;
			int drop=StringUtil.getInt(ss[location]);//掉落类型
			String pro=StringUtil.getString(ss[location+1]);//掉落物品
			int ability = StringUtil.getInt(ss[location+2]);//掉落几率
			try
			{
				if(!"".equals(pro) && ability>0)
				{
					if(drop ==1 || drop ==3 ||drop ==6)
					{
						String [] temp =pro.split(",");
						int dropId =StringUtil.getInt(temp[0]);
						int num =StringUtil.getInt(temp[1]);
					}
					drops.add(drop+"-"+pro+"-"+ability);
				}
			}
			catch (Exception e)
			{
				errorlogger.error("加载mission表出错,掉落数据格式错误"+id,e);
				System.exit(0);
			}
		}
		location = 14+7*6+3*2+3+6*3;
		specialtype1 =StringUtil.getInt(ss[location]);
		specialaward1 = StringUtil.getString(ss[location+1]);
		specialtype2 =StringUtil.getInt(ss[location+2]);
		specialaward2 = StringUtil.getString(ss[location+3]);
		bonusGrow = StringUtil.getInt(ss[location+4]);
		bonusReduce=StringUtil.getInt(ss[location+5]);
		lastTime = StringUtil.getInt(ss[location+6]);
		addtasktype =StringUtil.getInt(ss[location+7]);
		showtype =StringUtil.getInt(ss[location+8]);
		addtaskid =StringUtil.getInt(ss[location+9]);
		kopoint =StringUtil.getInt(ss[location+10]);
		addData();
	}
	
	public static CardJson[] getMonsterData(int targetId)
	{
		MissionData md=getMissionData(targetId);
		if(md==null)
		{
			return null;
		}
		CardJson[] cjs=new CardJson[6];
		for(int i=0;i<6;i++)
		{
			String[] ss=md.monsters[i].split("-");
			int cardId=StringUtil.getInt(ss[0]);
			int level=StringUtil.getInt(ss[1]);
			int atk=StringUtil.getInt(ss[2]);
			int def=StringUtil.getInt(ss[3]);
			int maxHp=StringUtil.getInt(ss[4]);
			int skillId=StringUtil.getInt(ss[5]);
			int bossMark=StringUtil.getInt(ss[6]);
			
			int criRate=0;
			int aviRate=0;
			int talent1=0;
			int talent2=0;
			int talent3=0;
			
			if(CardData.getData(cardId)==null)
			{
				continue;
			}
			cjs[i]=new CardJson(i, cardId, level, skillId, atk, def, maxHp, bossMark, criRate, aviRate, talent1, talent2, talent3);
		}
		return cjs;
	}
	
	public static MissionData getUnlockData(int missionId)
	{
		for(MissionData md : data.values())
		{
			if(md.unlockshowup==missionId)
			{
				return md;
			}
		}
		return null;
	}

	public int getSequence()
	{
		int result=0;
		for(int i=0;i<dataList.size();i++)
		{
			MissionData md=dataList.get(i);
			if(missiontype==md.missiontype && id>=md.id)
			{
				result++;
			}
		}
		return result;
	}
	
	/**获取一个区域的序列范围**/
	public static int[] getSequences(int map,int zone,int missionType)
	{
		//第一个元素普通关卡开始序列
		int[] result=new int[2];
		int k=0;
		for(int i=0;i<dataList.size();i++)
		{
			MissionData md=dataList.get(i);
			if(md.missiontype==missionType)
			{
				k++;
			}
			if(md.map==map && md.zone==zone && md.missiontype==missionType)
			{
				if(result[0]==0)
				{
					result[0]=k;
				}
				result[1]=k;
			}
		}
		return result;
	}
	
	public static List<String> getPreMapZoneMissionType(int missionId)
	{
		List<String> result=new ArrayList<String>();
		int mt=0;
		MissionData mdTemp=MissionData.getMissionData(missionId);
		if(mdTemp!=null)
		{
			mt=mdTemp.missiontype;
		}
		else
		{
			if(missionId==0)
			{
				mt=1;
			}
			else if (missionId==Constant.PveMissionType2)
			{
				mt=2;
			}
			else
			{
				return result;
			}
		}
		for(int i=0;i<dataList.size();i++)
		{
			MissionData md=dataList.get(i);
			if(md.missiontype==mt && md.id<=missionId)
			{
				String msg=md.map+","+md.zone+","+md.missiontype;
				if(!result.contains(msg))
				{
					result.add(msg);
				}
			}
		}
		return result;
	}
	
	
	/**所有关卡**/
	public static List<MissionData> getMissionDatas ()
	{
		List<MissionData> list =new ArrayList<MissionData>();
		for(MissionData missionData :data.values())
		{
			list.add(missionData);
		}
		for(int k=0;k<list.size();k++)
		{
			for(int j=list.size()-1;j>0;j--)
			{
				if(list.get(j).id<list.get(j-1).id)
				{
					MissionData missionData =list.get(j);
					list.set(j, list.get(j-1));
					list.set(j-1, missionData);
				}
			}
		}
		return list;
	}
	
	/**
	 * 是否可以掉指定材料
	 * lt@2014-5-10 下午08:25:24
	 * @param itemId
	 * @return
	 */
	public boolean canDropItemId(int itemId)
	{
		if(drops!=null && drops.size()>0)
		{
			for(String s:drops)
			{
				String[] ss=s.split("-");
				int type=StringUtil.getInt(ss[0]);
				if(type==1)
				{
					String[] temp=ss[1].split(",");
					int itemIdTemp=StringUtil.getInt(temp[0]);
					if(itemIdTemp==itemId)
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}

	/**
	 * 获取从当前完成的mission开始,最近解锁的num个可以掉落itemId的mission,倒序排列
	 * lt@2014-5-10 下午08:33:36
	 * @param missionId
	 * @param num
	 */
	public static List<Integer> getRecentUnlockMissions(int missionId,int num,int itemId)
	{
		List<Integer> result=new ArrayList<Integer>();
		MissionData md=getMissionData(missionId);
		if(md!=null)
		{
			List<MissionData> mdList=new ArrayList<MissionData>();
			for(MissionData mdTemp:dataList)
			{
				if(mdTemp.missiontype==md.missiontype && mdTemp.id<=md.id)
				{
					mdList.add(mdTemp);
				}
			}
			for(int i=mdList.size()-1;result.size()<num && i>=0;i--)
			{
				MissionData mdTemp=mdList.get(i);
				if(mdTemp.canDropItemId(itemId))
				{
					result.add(mdTemp.id);
				}
			}
		}
		return result;
	}
	
	/**
	 * 获取从当前完成的mission之后,即将解锁的num个可以掉落itemId的mission,顺序排列
	 * lt@2014-5-10 下午08:33:36
	 * @param missionId
	 * @param num
	 */
	public static List<Integer> getRecentLockMissions(int missionId,int num,int itemId)
	{
		List<Integer> result=new ArrayList<Integer>();
		MissionData md=getMissionData(missionId);
		if(md!=null)
		{
			List<MissionData> mdList=new ArrayList<MissionData>();
			for(MissionData mdTemp:dataList)
			{
				if(mdTemp.missiontype==md.missiontype && mdTemp.id>md.id)
				{
					mdList.add(mdTemp);
				}
			}
			for(int i=0;result.size()<num && i<mdList.size();i++)
			{
				MissionData mdTemp=mdList.get(i);
				if(mdTemp.canDropItemId(itemId))
				{
					result.add(mdTemp.id);
				}
			}
		}
		else if(md ==null && (missionId==0 || missionId==1))
		{
			List<MissionData> mdList=new ArrayList<MissionData>();
			for(MissionData mdTemp:dataList)
			{
				if(mdTemp.missiontype==2 && mdTemp.id>missionId)
				{
					mdList.add(mdTemp);
				}
			}
			for(int i=0;result.size()<num && i<mdList.size();i++)
			{
				MissionData mdTemp=mdList.get(i);
				if(mdTemp.canDropItemId(itemId))
				{
					result.add(mdTemp.id);
				}
			}
		}
		return result;
	}
}
