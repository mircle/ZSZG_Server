package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class PassiveSkillBasicExpData implements PropertyReader
{
	public int star;
	public int basicexp;
	
	private static HashMap<Integer, PassiveSkillBasicExpData> data = new HashMap<Integer, PassiveSkillBasicExpData>();
	public void addData()
	{
		data.put(star, this);
	}
	@Override
	public void resetData()
	{
		data.clear();
	}
	@Override
	public void parse(String[] ss)
	{
		
	}

	public static PassiveSkillBasicExpData getData(int star)
	{
		return data.get(star);
	}

}
