package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class MazeCostData implements PropertyReader
{
	public int number;//购买次数
	public int mazeid;//迷宫id
	public int viplevel;//vip等级
	public int cost;//花费水晶
	
	private static HashMap<Integer, MazeCostData> data =new HashMap<Integer, MazeCostData>();
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据number获取data**/
	public static MazeCostData getMazeCostData(int index)
	{
		return data.get(index);
	}

}
