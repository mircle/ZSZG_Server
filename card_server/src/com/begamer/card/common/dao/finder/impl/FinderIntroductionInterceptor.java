package com.begamer.card.common.dao.finder.impl;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.aop.IntroductionInterceptor;

import com.begamer.card.common.dao.finder.FinderExecutor;

public class FinderIntroductionInterceptor implements IntroductionInterceptor {
	private static final Logger logger = Logger
			.getLogger(FinderIntroductionInterceptor.class);

	@SuppressWarnings("unchecked")
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		FinderExecutor executor = (FinderExecutor) methodInvocation.getThis();

		String methodName = methodInvocation.getMethod().getName();
		if (methodName.startsWith("find") || methodName.startsWith("list")
				|| methodName.startsWith("get")) {
			logger.debug(this.getClass().getName()
					+ "	invoke		-	execute interceptor action!");
			Object[] arguments = methodInvocation.getArguments();
			return executor.executeFinder(methodInvocation.getMethod(),
					arguments);
		}
		/*
		 * else if( methodName.startsWith("iterate")) { Object[] arguments =
		 * methodInvocation.getArguments(); return
		 * executor.iteratorFinder(methodInvocation.getMethod(),arguments); }
		 * else if( methodName.startsWith("scroll")) { Object[] arguments =
		 * methodInvocation.getArguments(); return
		 * executor.scrollFinder(methodInvocation.getMethod(),arguments); }
		 */
		else {
			// logger.debug(this.getClass().getName() + " invoke - not execute
			// interceptor action!");
			return methodInvocation.proceed();
		}
	}

	@SuppressWarnings("unchecked")
	public boolean implementsInterface(Class intf) {
		return intf.isInterface()
				&& FinderExecutor.class.isAssignableFrom(intf);
	}
}