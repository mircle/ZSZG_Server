package com.begamer.card.model.pojo;

public class Item {
	private Integer id;
	private Integer playerId;
	private Integer itemId;
	private Integer pile;
	private Integer sell;
	private Integer newType;
	
	public static Item createItem(int playerId,int itemId,int pile)
	{
		Item i=new Item();
		i.setItemId(itemId);
		i.setPlayerId(playerId);
		i.setPile(pile);
		i.setSell(0);
		i.setNewType(0);
		return i;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPlayerId() {
		return playerId;
	}
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public void setPile(Integer pile)
	{
		this.pile = pile;
	}
	public Integer getPile()
	{
		return pile;
	}
	public Integer getSell() {
		return sell;
	}
	public void setSell(Integer sell) {
		this.sell = sell;
	}

	public Integer getNewType() {
		return newType;
	}

	public void setNewType(Integer newType) {
		this.newType = newType;
	}
	
	

}
